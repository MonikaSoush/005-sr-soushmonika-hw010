import axios from "axios";
import { createContext, useEffect, useState, useContext } from "react";

const client = axios.create({
  baseURL: "http://110.74.194.124:3034",
  headers: {
    "Content-Type": "application/json",
  },
});

const Context = createContext();

export function useProvider() {
  return useContext(Context);
}

function useProvideContext() {
  const [list, setList] = useState([]);
  const [message, setMessage] = useState();
  const [loading, setLoading] = useState();
  const [edit, setEdit] = useState();
  const isEdit = !!edit;

  useEffect(() => {
    getList();
  }, []);

  async function getList() {
    try {
      setLoading(true);
      setMessage("Loading...");
      const { data } = await client.get("/api/author");
      setList(data.data);
    } catch (err) {
      alert(err.message);
    } finally {
      setMessage(null);
      setLoading(false);
    }
  }
  async function add(value) {
    const json = { ...value };
    try {
      setLoading(true);
      setMessage("Saving...");
      if (json.file) {
        const form = new FormData();
        form.set("image", json.file);
        const { data } = await client.post("/api/images", form, {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        });
        console.log();
        delete json.file;
        json.image = data.url;
      }
      const { data } = await client.post("/api/author", JSON.stringify(json));
      setList([data.data, ...list]);
    } catch (err) {
      alert(err.message);
    } finally {
      setMessage(null);
      setLoading(false);
    }
  }
  async function del(value) {
    const json = { ...value };
    try {
      setLoading(true);
      setMessage("Deleting...");
      const { data } = await client.delete(`/api/author/${json._id}`);
      setList([...list.filter((item) => item._id !== data.data._id)]);
    } catch (err) {
      alert(err.message);
    } finally {
      setMessage(null);
      setLoading(false);
    }
  }
  async function update(value) {
    const json = { ...value };
    try {
      setLoading(true);
      setMessage("Update...");
      if (json.file) {
        const form = new FormData();
        form.set("image", json.file);
        const { data } = await client.post("/api/images", form, {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        });
        delete json.file;
        json.image = data.url;
      }
      const { data } = await client.put(
        `/api/author/${json._id}`,
        JSON.stringify(json)
      );
      setEdit(null);
      setList([
        ...list.map((item) => (item._id === data.data._id ? json : item)),
      ]);
    } catch (err) {
      alert(err.message);
    } finally {
      setMessage(null);
      setLoading(false);
    }
  }
  
  return {
    list,
    loading,
    edit,
    setEdit,
    add,
    update,
    message,
    isEdit,
    del,
  };
}

export default function (props) {
  const value = useProvideContext();
  return <Context.Provider value={value}>{props.children}</Context.Provider>;
}
