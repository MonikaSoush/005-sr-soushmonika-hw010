import React from "react";
import "./App.css";
import menu from "./components/menu";
import show from "./components/show";
import Provider from "./Provider";
import { Container } from "react-bootstrap";
import author from "./components/author";

export default function () {
  return (
    <Provider>
      <menu />
      <Container>
        <author />
        <show />
      </Container>
    </Provider>
  );
}
