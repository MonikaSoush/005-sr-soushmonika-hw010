import { Button, Table } from "react-bootstrap";
import { useProvider } from "../Provider";

export default function (props) {
  const { list, isEdit, setEdit, del } = useProvider();
  
  function renderRow(item, key) {
    return (
      <tr key={key}>
        <td>{key + 1}</td>
        <td>{item.name}</td>
        <td>{item.email}</td>
        <td className="image-col">
          {item.image ? (
            <img src={item.image} />
          ) : (
            <div className="noImage">
              <h6>No Image</h6>
            </div>
          )}
        </td>
        <td>
          <Button
            onClick={() => setEdit(item)}
            className="mr-2"
            variant="warning"
          >
            Edit
          </Button>
          <Button onClick={() => del(item)} variant="danger" disabled={isEdit}>
            Delete
          </Button>
        </td>
      </tr>
    );
  }
  return (
    <Table bordered className="mt-3">
      <thead>
        <tr>
          <th>#</th>
          <th>Author</th>
          <th>Email</th>
          <th>Image</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>{list.map(renderRow)}</tbody>
    </Table>
  );
}
