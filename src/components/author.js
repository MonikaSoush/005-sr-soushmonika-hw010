import { useFormik } from "formik";
import { useEffect } from "react";
import { Col, Form, Row, Button } from "react-bootstrap";
import * as Yup from "yup";
import { useProvider } from "../Provider";
export default function (props) {
  const { isEdit, edit: author, setEdit, add, update } = useProvider();
  const validationSchema = Yup.object().shape({
    name: Yup.string()
      .matches("^[a-zA-Z\\s]+$", "Author Name allow character only")
      .required("Author Name is required"),
    email: Yup.string().email("Email is invalid").required("Email is required"),
  });
  const {
    values,
    handleChange,
    handleSubmit,
    errors,
    setFieldValue,
    setValues,
  } = useFormik({
    validationSchema,
    onSubmit,
    initialValues: isEdit ? author : getDefualt(),
    validateOnChange: false,
  });
  function getDefualt() {
    return {
      name: "",
      email: "",
      file: null,
      image: "",
    };
  }
  useEffect(() => {
    setValues(isEdit ? author : getDefualt());
  }, [isEdit, author]);

  async function onSubmit(values, { resetForm }) {
    try {
      if (isEdit) {
        await update(values);
      } else {
        await add(values);
      }
      resetForm();
    } catch (error) {}
  }
  
  return (
    <div {...props}>
      <h1 className="font-weight-normal my-3">Author</h1>
      <Form onSubmit={handleSubmit}>
        <Row>
          <Col className="col-12 col-lg-9">
            <Form.Group>
              <Form.Label>Author Name</Form.Label>
              <Form.Control
                type="text"
                onChange={handleChange}
                value={values.name}
                placeholder="Enter Author Name"
                name="name"
                isInvalid={!!errors.name}
              />
              <Form.Control.Feedback type="invalid">
                {errors.name}
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group>
              <Form.Label>Email</Form.Label>
              <Form.Control
                type="text"
                onChange={handleChange}
                value={values.email}
                placeholder="Enter email"
                name="email"
                isInvalid={!!errors.email}
              />
              <Form.Control.Feedback type="invalid">
                {errors.email}
              </Form.Control.Feedback>
            </Form.Group>
            <div className="d-flex justify-content-end">
              <Button variant={isEdit ? "info" : "primary"} type="submit">
                {isEdit ? "Save" : "Submit"}
              </Button>
              {isEdit && (
                <Button
                  className="ml-3"
                  variant="secondary"
                  onClick={() => setEdit(null)}
                >
                  Cancel
                </Button>
              )}
            </div>
          </Col>
          <Col className="col-5 col-lg-3">
            <div className="border image mb-2">
              {values.file || values.image ? (
                <img
                  src={
                    values.file
                      ? URL.createObjectURL(values.file)
                      : values.image
                  }
                />
              ) : (
                <div className="noImage">
                  <h6>No Image</h6>
                </div>
              )}
            </div>
            <Form.Group className="w-100">
              <Form.Label>Choose Image</Form.Label>
              <Form.Control
                type="file"
                onChange={(e) =>
                  setFieldValue("file", e.currentTarget.files[0])
                }
              />
            </Form.Group>
          </Col>
        </Row>
      </Form>
    </div>
  );
}
